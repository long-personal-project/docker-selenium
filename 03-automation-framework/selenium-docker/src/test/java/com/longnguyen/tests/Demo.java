package com.longnguyen.tests;

import com.longnguyen.util.Config;
import com.longnguyen.util.Constants;

public class Demo extends AbstractTest{
    public static void main(String[] args) {
        Config.initialize();
        String urlFormat = Config.get(Constants.GRID_URL_FORMAT);
        String hubHost = Config.get(Constants.GRID_HUB_HOST);
        String url = String.format(urlFormat, hubHost);
        log.info("grid url: {}", url);
    }


}
